
//When the document is uploaded.
$(document).ready(function(){

  $("#dropDown").click(function(){
    $("#Move").slideToggle("slow");
    $("#Hide").slideToggle("slow");
    $("#Show").slideToggle("slow");
    $("#fadeIn").slideToggle("slow");
  });

  //Alligning the square so it would be 250px from the left.
  $("#Move").click(function(){
    $("#Styles").animate({
      left : '250px'
    });
  });

//Hiding the square.
$("#Hide").click(function(){
  $("#Styles").hide();
});

//Showing the hidden square
$("#Show").click(function(){
  $("#Styles").show();
});

//Switching two more square in and out
$("#fadeIn").click(function(){
  $("#Styles_1").fadeToggle(1000);
  $("#Styles_2").fadeToggle(1000);
});

});
