var id;//Book Id
var bookName; //save the book name

//Generates a 3 digit book ID
function ID_generator(){
id = Math.floor((Math.random() * 999) + 100);
while(id.toString().length != 3){ //Make sure that the id is 3 digits
  id = Math.floor((Math.random() * 999) + 100);
}
 console.log("Book ID is: " + id);
return id;
};

//Saves the name of the user who borrowed the book.
function borrowedBy(user){
localStorage.setItem("user", user);
};

//Check the availability of the chosen book. take the book as a parameter.
function availability(available){
  if(!available){
    localStorage.setItem("available", 0);
    console.log("The book " + available + " is borrowed by a student.");
    return false
  }else{
    localStorage.setItem("available", 1);
    console.log("The book " + available + " is available");
    return true;
  }
};

/*
addBook("Yastawat", "art");

//Add a book to a specefic shelf. FIX-ME
function addBook(name, category){
  if(category.toLowerCase() == "art" ){
    $('#shelves').append(name);
  }else if(category.toLowerCase() == "science"){
  }
};
